package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
  @Test
  public void decreaseCounterTest1() throws Exception {
    int counter = new Increment().decreasecounter(1);
    assertEquals("Counter",1,counter);
  }

   @Test
  public void decreaseCounterTest2() throws Exception {
    int counter = new Increment().decreasecounter(7);
    assertEquals("Counter",1,counter);
  }
 @Test
  public void decreaseCounterTest3() throws Exception {
    int counter = new Increment().decreasecounter(0);
    assertEquals("Counter",1,counter);
  }


}
